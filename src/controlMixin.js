export const controlMixin = {
    props : {
        label: { type: String, default: ''},
        name: { type: String, default: ''},
//        params: { type: Object, default() {return {}}},
//        validation: { type: Boolean, default: false },
//        pattern: { type: String, default: /.*/ },
//        list: { type: Object, default() {return {}} },
        requiredMessage: { type:String },
        value: { type: String },
        required: { type: String, default: "false"},
//        defaultValue: { type: String, default: ''}     
    },
    data() {
        return {
            currentValue: this.value ? this.value : '',
            requiredErrorMsg: this.requiredMessage ? this.requiredMessage : 'This field is required',
            hasError: false,
            errorMsg:''
        }
    },
    methods: {
        validate(event) {            
            if (!this.currentValue) {
                event.preventDefault();
                this.hasError = true;
                this.errorMsg = this.requiredErrorMsg;
                return false;
            }
        }
    },
    mounted() {
        if(parseBool(this.required)) {
            let node = this.$el;
            while (node) { 
                if (node.tagName === 'FORM')  {
                    node.addEventListener("submit", this.validate);
                    return; 
                }
                else node = node.parentElement; 
            }
        }
    }
}