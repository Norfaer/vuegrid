import Vue from 'vue'
import Enumerator from './classes/enumerator.js'

import vgButton from './components/Button.vue';

import vgTabs from './components/Tabs.vue';
import vgTab from './components/Tab.vue';

import vgAccordion from './components/Accordion.vue';
import vgAccordions from './components/Accordions.vue';

import vgRange from './components/Range.vue';
import vgSwitch from './components/Switch.vue';
import vgDatepicker from './components/Datepicker.vue';
import vgMask from './components/Mask.vue';
import vgCollection from './components/Collection.vue';
import vgInput from './components/Input.vue';
import vgRadio from './components/Radio.vue';
import vgSelect from './components/Select.vue';
import vgCheck from './components/Check.vue';
import vgModal from './components/Modal.vue';

Vue.component('vg-button', vgButton);
Vue.component('vg-tabs', vgTabs);
Vue.component('vg-tab', vgTab);
Vue.component('vg-accordion', vgAccordion);
Vue.component('vg-accordions', vgAccordions);
Vue.component('vg-range', vgRange);
Vue.component('vg-switch', vgSwitch);
Vue.component('vg-datepicker', vgDatepicker);
Vue.component('vg-mask', vgMask);
Vue.component('vg-collection', vgCollection);
Vue.component('vg-input', vgInput);
Vue.component('vg-radio', vgRadio);
Vue.component('vg-select', vgSelect);
Vue.component('vg-check', vgCheck);
Vue.component('vg-modal', vgModal);


window.parseBool = function (b) {
  return b==='true' || b==='on' || b==='yes' || b===true;
}


document.addEventListener('DOMContentLoaded', function() {
  window.Enumerator = new Enumerator();
  var apples = document.getElementsByClassName('vg-app');
  for(let seed of apples) {
    new Vue({
      el: seed,
    });
  }
});