import  Vue  from 'vue'
import { EventBus } from './eventBus'

export const togglerMixin = {
    props : {
    },
    data() {
        return {
            toggleables:[],
            hasToggled:false
        }
    },
    methods: {
        add(what) {
            if (what.isActive) {
                what.isActive = this.hasToggled ? false : what.isActive;
                this.hasToggled = true;
            }
            this.toggleables.push(what);
        },
        switchTo(to) {
            for(let t of this.toggleables) {
                t.isActive = (t === to); 
            }
            Vue.nextTick(()=>{ EventBus.$emit('toggled') });            
        }
    },
    mounted() {
    }
}