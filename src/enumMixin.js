export const enumMixin = {
    props : {
        id:{ type: String, default: '' }
    },
    data(){
        return {
            uniqueId: this.id ? this.id : Enumerator.id
        }
    },
}