'use strict';

export default class Enumerator {
    constructor(prefix = '_e_id_'){                    
        this.seed = prefix + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        this.counter = 0
    }
    get id() {
        return this.seed + (this.counter++)
    }
    set id(value) {}
}

